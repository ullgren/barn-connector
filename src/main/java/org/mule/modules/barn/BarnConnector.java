/**
 * (c) 2003-2015 MuleSoft, Inc. The software in this package is published under the terms of the CPAL v1.0 license,
 * a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.barn;

import org.mule.api.annotations.ConnectionStrategy;
import org.mule.api.annotations.Connector;
import org.mule.api.annotations.Configurable;
import org.mule.api.annotations.Processor;
import org.mule.api.annotations.display.FriendlyName;
import org.mule.api.annotations.display.Placement;
import org.mule.api.annotations.param.Default;
import org.mule.modules.barn.strategy.ConnectorConnectionStrategy;

/**
 * Anypoint Connector
 *
 * @author MuleSoft, Inc.
 */
@Connector(name="barn", friendlyName="Barn")
public class BarnConnector
{
    /**
     * Configurable
     */
    @Configurable
    @Default("value")
    private String myProperty;

    @ConnectionStrategy
    ConnectorConnectionStrategy connectionStrategy;

    /**
     * Custom processor that places an animal in the barn.
     *
     * {@sample.xml ../../../doc/barn-connector.xml.sample barn:put-in-barn}
     *
     * @param animal Name of the animal to be place in the barn
     * @return returns processed message
     */
    @Processor(friendlyName="Put an animal in the barn")
    public String putInBarn(@Placement(group="Parameters")
      @FriendlyName("The animal's name") String animal) {
    	
    		if ( connectionStrategy.getInspector() != null ) {
    			connectionStrategy.getInspector().inspect(animal);
    		}
    		connectionStrategy.getPens().add(animal);
    		
            return animal + " has been placed in the barn";
    }

    /**
     * Set property
     *
     * @param myProperty My property
     */
    public void setMyProperty(String myProperty) {
        this.myProperty = myProperty;
    }

    /**
     * Get property
     */
    public String getMyProperty() {
        return this.myProperty;
    }

    public ConnectorConnectionStrategy getConnectionStrategy() {
        return connectionStrategy;
    }

    public void setConnectionStrategy(ConnectorConnectionStrategy connectionStrategy) {
        this.connectionStrategy = connectionStrategy;
    }

}