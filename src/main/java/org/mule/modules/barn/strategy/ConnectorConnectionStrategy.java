/**
 * (c) 2003-2015 MuleSoft, Inc. The software in this package is published under the terms of the CPAL v1.0 license,
 * a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.modules.barn.strategy;

import java.util.ArrayList;

import org.mule.api.ConnectionException;
import org.mule.api.annotations.Configurable;
import org.mule.api.annotations.Connect;
import org.mule.api.annotations.ConnectionIdentifier;
import org.mule.api.annotations.Disconnect;
import org.mule.api.annotations.TestConnectivity;
import org.mule.api.annotations.ValidateConnection;
import org.mule.api.annotations.components.ConnectionManagement;
import org.mule.api.annotations.display.Placement;
import org.mule.api.annotations.param.ConnectionKey;
import org.mule.api.annotations.param.Default;
import org.mule.api.annotations.param.Optional;

/**
 * ConnectionManagement type Strategy
 *
 * @author MuleSoft, Inc.
 */
@ConnectionManagement(configElementName = "config-type", friendlyName = "Configuration type strategy")
public class ConnectorConnectionStrategy
{
    /**
     * Configurable
     */
    @Configurable
    @Default("value")
    private String myStrategyProperty;
    
    private ArrayList<String> pens;
    
    @Configurable
    @Optional 
    @Placement(tab="Advanced") 
    private AnimalHealthInspector inspector;

    
    @Connect
    @TestConnectivity
    public void connection(@ConnectionKey Integer initialCapacity) throws ConnectionException  {
    	pens = new ArrayList<>(initialCapacity);
    }
    
    @Disconnect
    public void disconnect() {
    	
    }
    
    @ConnectionIdentifier
    public String connectionId() {
    	return "barn";
    }
    
    @ValidateConnection
    public boolean isConnected() {
    	return this.pens != null;
    }
    
    public ArrayList<String> getPens() {
		return pens;
	}
    
    public void setPens(ArrayList<String> pens) {
		this.pens = pens;
	}
    
    public AnimalHealthInspector getInspector() {
		return inspector;
	}
    
    public void setInspector(AnimalHealthInspector inspector) {
		this.inspector = inspector;
	}
    /**
     * Set strategy property
     *
     * @param myStrategyProperty my strategy property
     */
    public void setMyStrategyProperty(String myStrategyProperty) {
        this.myStrategyProperty = myStrategyProperty;
    }

    /**
     * Get property
     */
    public String getMyStrategyProperty() {
        return this.myStrategyProperty;
    }

}