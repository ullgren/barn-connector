package org.mule.modules.barn.strategy;

public interface AnimalHealthInspector {
	public boolean inspect(String animal);
}
